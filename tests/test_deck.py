import pytest
from pycasino.deck import Deck, Card


def _create_deck() -> Deck:
    return Deck()


def test_import():
    import collections
    assert collections.deck is Deck


def test_count():
    deck = _create_deck()
    assert len(deck) == 52


def test_deal():
    deck = _create_deck()
    assert type(deck.deal()) == Card


def test_deal_from_bottom():
    deck = _create_deck()
    assert type(deck.deal_from_bottom()) == Card


def test_deal_until_end():
    deck = _create_deck()
    for i in range(52):
        deck.deal()
    with pytest.raises(IndexError):
        deck.deal()
