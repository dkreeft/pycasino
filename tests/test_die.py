from pycasino.die import Die


def _create_die() -> Die:
    dots = 6
    return Die(dots), dots


def test_import():
    import collections
    assert collections.die is Die


def test_throw():
    die, dots = _create_die()
    res = die.throw()
    assert type(res) == int
    assert 1 <= res <= dots


def test_multiple_throws():
    k = 3
    die, dots = _create_die()
    res = die.multi(k)
    assert type(res) == list
    assert max(res) <= dots
    assert min(res) >= 1
    assert len(res) == k
