<p align="center">
<a href="https://gitlab.com/dkreeft/pycasino">
<div style="width: 200px;"><img align="center" src="https://gitlab.com/dkreeft/pycasino/-/raw/master/logo.svg"/></div>
</a>
</p>

# pycasino - casino games in Python
pycasino is a Python library and command-line utility aiming to recreate several [casino games](https://en.wikipedia.org/wiki/Casino_game). The idea for this library is based on the book [Hands-On Programming with R](https://rstudio-education.github.io/hopr/) by Garret Grolemund (available for free). The implementation of the Deck class is based on [`deck`](https://github.com/zooba/deck).

## Installation
[pip](https://pip.pypa.io/en/stable/) can be used to install pycasino:

```bash
pip install pycasino
```

However, we recommend to install pycasino using [pipx](https://github.com/pipxproject/pipx) to make it available outside your virtual environment:

```bash
pipx install pycasino
```

## Usage
pycasino can be used as a command-line utility as follows:

```bash
pycasino <dir>
```

## Contributing
Please refer to [CONTRIBUTING.md](https://gitlab.com/dkreeft/pycasino/-/blob/master/CONTRIBUTING.md)

## License
[BSD-3](https://gitlab.com/dkreeft/pycasino/-/blob/master/LICENSE)

